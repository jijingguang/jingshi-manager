package com.bootdo.common.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.bootdo.common.config.BootdoConfig;
import com.bootdo.common.domain.FileDO;
import com.bootdo.common.utils.FileType;
import com.bootdo.common.utils.FileUtil;

@Controller 
@RequestMapping("/common/qweq")
public class CommonController {
	
	@Value(value="classpath:config.json")
    private Resource resource;
	
	@Autowired
	private BootdoConfig bootdoConfig;
	
    //ueditr后端配置
    @ResponseBody
    @RequestMapping(value = "/upload",headers = "Accept=application/json")
    public String ueditor(@RequestParam("action") String action, @RequestParam("noCache") String nocache, HttpServletRequest request, HttpServletResponse response){
        try {
            response.setContentType("application/json;charset=utf-8");
//            Resource resource = new ClassPathResource("config.json");
            org.springframework.core.io.Resource res = new ClassPathResource("config.json");
            
            BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
            StringBuffer message=new StringBuffer();
            String line = null;
            while((line = br.readLine()) != null) {
                message.append(line);
            }
//            String result = message.toString().replaceAll("/\\*(.|[\\r\\n])*?\\*/","");
//            JSONObject json = JSONObject.fromObject(result);
//            PrintWriter out = response.getWriter();
//            out.print(json.toString());
            
            
            return message.toString();
        }catch (Exception e){
            e.printStackTrace();
            return e.toString();
        }
    }
    @ResponseBody
    @RequestMapping(value = "/upload1",headers = "Accept=application/json")
    public void ueditor1(@RequestParam("upfile") MultipartFile[] files, HttpServletRequest request, HttpServletResponse response)throws Exception{
    	
    	JSONObject json = new JSONObject();
    	
    	System.out.println("上传图片！");
    	for (MultipartFile file : files) {
    		
    		PrintWriter out = response.getWriter();
    		
    		String fileName = file.getOriginalFilename();
    		fileName = FileUtil.renameToUUID(fileName);
    		FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
        	
    		 try {
    			 FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
    	            //写入文件
    	            json.put("state", "SUCCESS");
    	            json.put("title", file.getName());
    	            json.put("url", bootdoConfig.getDomainName()+sysFile.getUrl());// 图片访问 相对路径
    	            json.put("original", file.getName());
    	            out.print(json.toString());
    	        } catch (Exception e) {
    	            json.put("state", "ERROR");
    	            throw new Exception("上传文件失败！");
    	        }finally {
    	        	out.close();
    	        }
    	        out.print(json.toString());
		}
    	
    }
    
    
}
