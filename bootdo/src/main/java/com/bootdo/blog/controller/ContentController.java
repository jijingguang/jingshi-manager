package com.bootdo.blog.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bootdo.blog.domain.ArticleDO;
import com.bootdo.blog.domain.CategoryDO;
import com.bootdo.blog.service.CategoryService;
import com.bootdo.blog.service.ContentService;
import com.bootdo.common.config.BootdoConfig;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.domain.DictDO;
import com.bootdo.common.domain.FileDO;
import com.bootdo.common.utils.DateUtils;
import com.bootdo.common.utils.FileType;
import com.bootdo.common.utils.FileUtil;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 文章内容
 * 
 */
@Controller
@RequestMapping("/blog/bContent")
public class ContentController extends BaseController {
	@Autowired
    ContentService bContentService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private BootdoConfig bootdoConfig;

	@GetMapping()
	@RequiresPermissions("blog:bContent:bContent")
	String bContent() {
		return "blog/bContent/bContent";
	}

	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("blog:bContent:bContent")
	public PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		
		List<ArticleDO> bContentList = bContentService.queryArticleList(query);
//		for (ArticleDO articleDO : bContentList) {
//			if(StringUtils.isNoneEmpty(articleDO.getImageUrl())) {
//				articleDO.setImageUrl(articleDO.getImageUrl());
//			}
//		}
		int total = bContentService.queryCountArticleList(query);
		PageUtils pageUtils = new PageUtils(bContentList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	@RequiresPermissions("blog:bContent:add")
	String add() {
		return "blog/bContent/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("blog:bContent:edit")
	String edit(@PathVariable("id") Long id, Model model) {
		ArticleDO article = bContentService.getArticle(id);
//		if(StringUtils.isNoneEmpty(article.getImageUrl())) {
//			article.setImageUrl(article.getImageUrl());
//		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		List<CategoryDO> queryCategoryList = categoryService.queryCategoryList(params);
		for (CategoryDO categoryDO : queryCategoryList) {
			if(categoryDO.getParentId() !=0) {
				categoryDO.setCategoryName("&nbsp;&nbsp;&nbsp;&nbsp"+categoryDO.getCategoryName());
			}
		}
		
		model.addAttribute("article", article);
		model.addAttribute("queryCategoryList", queryCategoryList);
		return "blog/bContent/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@RequiresPermissions("blog:bContent:add")
	@PostMapping("/save")
	public R save(ArticleDO article ) {   //,@RequestParam("file") MultipartFile file
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		
//		if(StringUtils.isNotBlank(file.getOriginalFilename())) {
//			String fileName = file.getOriginalFilename();
//			fileName = FileUtil.renameToUUID(fileName);
//			FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
//			try {
//				FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
//			} catch (Exception e) {
//				return R.error();
//			}
//			
//			article.setImageUrl(sysFile.getUrl());
//		}
		
		if(article.getIsOpen() == null) {
			article.setIsOpen(0);
		}
		
		article.setIsDelete(0);
		article.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		article.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		int count;
		if (article.getId() == null) {
			count = bContentService.saveArticle(article);
		} else {
			count = bContentService.updateArticle(article);
		}
		if (count > 0) {
			return R.ok().put("id", article.getId());
		}
		return R.error();
	}
	
	/**
	 * 修改
	 */
	@RequiresPermissions("blog:bContent:edit")
	@ResponseBody
	@RequestMapping("/update")
	public R update( ArticleDO article ) { //,@RequestParam("file") MultipartFile file
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		
//		if(StringUtils.isNotBlank(file.getOriginalFilename())) {
//			String fileName = file.getOriginalFilename();
//			fileName = FileUtil.renameToUUID(fileName);
//			FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
//			try {
//				FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
//			} catch (Exception e) {
//				return R.error();
//			}
//			
//			article.setImageUrl(sysFile.getUrl());
//		}
//		
		if(article.getId() == null) {
			return R.error(1,"id为空！");
		}
		article.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		bContentService.updateArticle(article);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@RequiresPermissions("blog:bContent:remove")
	@PostMapping("/remove")
	@ResponseBody
	public R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (bContentService.removeArticle(id) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/**
	 * 批量删除
	 */
	@RequiresPermissions("blog:bContent:batchRemove")
	@PostMapping("/batchRemove")
	@ResponseBody
	public R remove(@RequestParam("ids[]") Long[] ids) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		bContentService.batchRemoveArticle(ids);
		return R.ok();
	}
	
	@GetMapping("/category")
	@ResponseBody
	public List<CategoryDO> listCategory(@RequestParam Map<String, Object> params) {
		
		 List<CategoryDO> queryCategoryList = categoryService.queryCategoryList(params);
		 
		 for (CategoryDO categoryDO : queryCategoryList) {
			if(categoryDO.getParentId() !=0) {
				categoryDO.setCategoryName("&nbsp;&nbsp;&nbsp;&nbsp"+categoryDO.getCategoryName());
			}
		}
		 
		return queryCategoryList;
	};
	
}
