package com.bootdo.blog.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.blog.domain.ConsultModel;
import com.bootdo.blog.service.ConsultService;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;

@Controller
@RequestMapping("/blog/consult")
public class ConsultController extends BaseController{
	
	@Autowired
	private ConsultService consultService;
	
	@GetMapping()
	@RequiresPermissions("blog:consult:consultList")
	public String consultList() {
		return "blog/bContent/consult/consultList";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("blog:consult:consultList")
	public PageUtils queryConsultList(@RequestParam Map<String, Object> params) {
		
		Query query = new Query(params);
		
		List<ConsultModel> list = consultService.queryConsultList(query);
		
		int total = consultService.queryCountConsultList(query);
		
		
		PageUtils pageUtils = new PageUtils(list, total);
		return pageUtils;
		
	}
	
//	@GetMapping()
//	@RequiresPermissions("blog:consult:consultList")
//	String Consult(){
//	    return "blog/bContent/consult/consultList";
//	}
//	
//	@ResponseBody
//	@GetMapping("/list")
//	@RequiresPermissions("blog:consult:consultList")
//	public PageUtils list(@RequestParam Map<String, Object> params){
//		//查询列表数据
//        Query query = new Query(params);
//		List<ConsultDO> consultList = consultService.list(query);
//		int total = consultService.count(query);
//		PageUtils pageUtils = new PageUtils(consultList, total);
//		return pageUtils;
//	}

}
