package com.bootdo.blog.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.blog.domain.CategoryDO;
import com.bootdo.blog.service.CategoryService;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.DateUtils;
import com.bootdo.common.utils.R;

@Controller
@RequestMapping("/blog/category")
public class CategoryController extends BaseController{
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping()
	@RequiresPermissions("blog:category:categoryList")
	String categoryList() {
		return "blog/bContent/category/categoryList";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("blog:category:categoryList")
	public List<CategoryDO> list(@RequestParam Map<String, Object> params) {
		
		List<CategoryDO> categoryList = categoryService.queryCategoryList(params);
//		int total = categoryService.queryCountArticleList(params);
//		PageUtils pageUtils = new PageUtils(categoryList, total);
		return categoryList;
	}
	
	/**
	 * 去添加页面
	 * @return
	 */
	@GetMapping("/add/{pId}")
	@RequiresPermissions("blog:category:add")
	String addCategory(Model model, @PathVariable("pId") Long pId) {
		
		model.addAttribute("pId", pId);
		if (pId == 0) {
			model.addAttribute("pName", "根目录");
		} else {
			model.addAttribute("pName", categoryService.getCategory(pId).getCategoryName());
		}
		
		return "blog/bContent/category/add";
	}
	
	/**
	 * 添加保存
	 */
	@ResponseBody
	@RequiresPermissions("blog:category:add")
	@PostMapping("/save")
	public R addCategory(CategoryDO category) {
		
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		category.setIsDelete(0);
		category.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		category.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int count = categoryService.addCategory(category);
		
		if (count > 0) {
			return R.ok().put("id", category.getId());
		}
		
		return R.error();
	}
	
	/**
	 * 去修改页面
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/edit/{id}")
	@RequiresPermissions("blog:category:edit")
	String edit( @PathVariable("id") Long id, Model model) {
		CategoryDO category = categoryService.getCategory(id);
		Long pId = category.getParentId();
		model.addAttribute("pId", pId);
		if (pId == 0) {
			model.addAttribute("pName", "根目录");
		} else {
			model.addAttribute("pName",categoryService.getCategory(pId).getCategoryName());
		}
		model.addAttribute("category", category);
		return "blog/bContent/category/edit";
	}
	
	/**
	 * 修改
	 */
	@RequiresPermissions("blog:category:edit")
	@ResponseBody
	@RequestMapping("/update")
	public R update( CategoryDO category) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if(category.getId() == null) {
			return R.error(1,"id为空！");
		}
		category.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		categoryService.updateCategory(category);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequiresPermissions("blog:category:remove")
	@PostMapping("/remove")
	@ResponseBody
	public R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		
		CategoryDO category = categoryService.queryCategoryByPid(id);
		
		if(category != null) {
			return R.error(1, "该分类有下级标签请先删除下级分类！");
		}
		
		if (categoryService.removeCategory(id) > 0) {
			return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 批量删除
	 */
	@RequiresPermissions("blog:category:batchRemove")
	@PostMapping("/batchRemove")
	@ResponseBody
	public R batchRemove(@RequestParam("ids[]") Long[] ids) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		categoryService.batchRemoveCategory(ids);
		return R.ok();
	}

}
