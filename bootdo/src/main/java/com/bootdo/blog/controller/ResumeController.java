package com.bootdo.blog.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.blog.domain.ResumeDO;
import com.bootdo.blog.service.ResumeService;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.DateUtils;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

@Controller
@RequestMapping("/blog/resume")
public class ResumeController  extends BaseController{
	
	@Autowired
	private ResumeService resumeService;

	
	@GetMapping()
	@RequiresPermissions("blog:resume:resumeList")
	String categoryList() {
		return "blog/bContent/resume/resumeList";
	}
	
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("blog:resume:resumeList")
	public PageUtils queryResumeList(@RequestParam Map<String, Object> params) {
		
		Query query = new Query(params);
		
		List<ResumeDO> list = resumeService.queryResumeList(query);
		
		int total = resumeService.queryCountResumeList(query);
		
		
		PageUtils pageUtils = new PageUtils(list, total);
		return pageUtils;
		
	}
	
	@GetMapping("/add")
	@RequiresPermissions("blog:resume:add")
	String add() {
		return "blog/bContent/resume/add";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@RequiresPermissions("blog:resume:add")
	@PostMapping("/save")
	public R save(ResumeDO resume ) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		
		resume.setIsDelete(0);
		resume.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		resume.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int count = resumeService.saveResume(resume);
		if (count > 0) {
			return R.ok().put("id", resume.getId());
		}
		
		return R.error();
	}
	
	@GetMapping("/edit/{id}")
	@RequiresPermissions("blog:resume:edit")
	String edit(@PathVariable("id") Long id, Model model) {
		
		ResumeDO resume = resumeService.getResumeById(id);
		
		model.addAttribute("resume", resume);
		return "blog/bContent/resume/edit";
	}
	
	
	/**
	 * 修改
	 */
	@RequiresPermissions("blog:resume:edit")
	@ResponseBody
	@RequestMapping("/update")
	public R update( ResumeDO resume) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		
		resume.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		resumeService.updateResume(resume);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequiresPermissions("blog:resume:remove")
	@PostMapping("/remove")
	@ResponseBody
	public R remove(Long id) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (resumeService.removeResume(id) > 0) {
			return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 批量删除
	 */
	@RequiresPermissions("blog:resume:batchRemove")
	@PostMapping("/batchRemove")
	@ResponseBody
	public R remove(@RequestParam("ids[]") Long[] ids) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		resumeService.batchRemoveResume(ids);
		return R.ok();
	}
	
}
