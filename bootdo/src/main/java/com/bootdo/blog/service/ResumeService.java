package com.bootdo.blog.service;

import java.util.List;
import java.util.Map;

import com.bootdo.blog.domain.ResumeDO;

public interface ResumeService {
	
	List<ResumeDO> queryResumeList(Map<String, Object> map);
	
	int queryCountResumeList(Map<String, Object> map);
	
	ResumeDO getResumeById(Long id);
	
	int saveResume(ResumeDO resume);
	
	int updateResume(ResumeDO resume);
	
	int removeResume(Long id);
	
	int batchRemoveResume(Long[] ids);

}
