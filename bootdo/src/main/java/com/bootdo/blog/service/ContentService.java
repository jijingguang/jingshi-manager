package com.bootdo.blog.service;

import java.util.List;
import java.util.Map;

import com.bootdo.blog.domain.ArticleDO;
import com.bootdo.blog.domain.ContentDO;

/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
public interface ContentService {
	
	ContentDO get(Long cid);
	
	ArticleDO getArticle(Long id);
	
	List<ArticleDO> queryArticleList(Map<String, Object> map);
	
	int queryCountArticleList(Map<String, Object> map);
	
	List<ContentDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ContentDO bContent);
	
	int update(ContentDO bContent);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
	
	int removeArticle(Long id);
	
	int saveArticle(ArticleDO article);
	
	int updateArticle(ArticleDO article);
	
	int batchRemoveArticle(Long[] ids);
}
