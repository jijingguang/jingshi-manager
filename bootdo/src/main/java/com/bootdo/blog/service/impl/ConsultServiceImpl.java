package com.bootdo.blog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootdo.blog.dao.ConsultDao;
import com.bootdo.blog.domain.ConsultDO;
import com.bootdo.blog.domain.ConsultModel;
import com.bootdo.blog.service.ConsultService;


@Service
public class ConsultServiceImpl implements ConsultService {
	
	@Autowired
	private ConsultDao consultDao;

	@Override
	public List<ConsultModel> queryConsultList(Map<String, Object> params) {
		
		return consultDao.queryConsultList(params);
	}

	@Override
	public int queryCountConsultList(Map<String, Object> params) {
		
		return consultDao.queryCountConsultList(params);
	}
	
	@Override
	public List<ConsultDO> list(Map<String, Object> map){
		return consultDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return consultDao.count(map);
	}

}
