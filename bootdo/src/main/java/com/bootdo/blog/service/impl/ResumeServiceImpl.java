package com.bootdo.blog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootdo.blog.dao.ResumeDao;
import com.bootdo.blog.domain.ResumeDO;
import com.bootdo.blog.service.ResumeService;


@Service
public class ResumeServiceImpl implements ResumeService {
	
	@Autowired
	private ResumeDao resumeMapper;

	@Override
	public List<ResumeDO> queryResumeList(Map<String, Object> map) {
		
		return resumeMapper.queryResumeList(map);
	}

	@Override
	public int queryCountResumeList(Map<String, Object> map) {
		
		return resumeMapper.queryCountResumeList(map);
	}

	@Override
	public ResumeDO getResumeById(Long id) {
		
		return resumeMapper.getResumeById(id);
	}

	@Override
	public int saveResume(ResumeDO resume) {
		
		return resumeMapper.saveResume(resume);
	}

	@Override
	public int updateResume(ResumeDO resume) {
		
		return resumeMapper.updateResume(resume);
	}

	@Override
	public int removeResume(Long id) {
		
		return resumeMapper.removeResume(id);
	}

	@Override
	public int batchRemoveResume(Long[] ids) {
		
		return resumeMapper.batchRemoveResume(ids);
	}

}
