package com.bootdo.blog.service;

import java.util.List;
import java.util.Map;

import com.bootdo.blog.domain.ConsultDO;
import com.bootdo.blog.domain.ConsultModel;

public interface ConsultService {
	
	List<ConsultModel> queryConsultList(Map<String, Object> params);
	
	int queryCountConsultList(Map<String, Object> params);
	
	List<ConsultDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);

}
