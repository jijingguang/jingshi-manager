package com.bootdo.blog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootdo.blog.dao.CategoryDao;
import com.bootdo.blog.domain.CategoryDO;
import com.bootdo.blog.service.CategoryService;


@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryDao categoryDao;

	@Override
	public List<CategoryDO> queryCategoryList(Map<String, Object> map) {
		
		return categoryDao.queryCategoryList(map);
	}

	@Override
	public int queryCountArticleList(Map<String, Object> map) {
		
		return categoryDao.queryCountArticleList(map);
	}

	@Override
	public int addCategory(CategoryDO category) {
		
		return categoryDao.addCategory(category);
	}

	@Override
	public CategoryDO getCategory(Long id) {
		
		return categoryDao.getCategory(id);
	}

	@Override
	public int updateCategory(CategoryDO category) {
		
		return categoryDao.updateCategory(category);
	}

	@Override
	public int removeCategory(Long id) {
		
		return categoryDao.removeCategory(id);
	}

	@Override
	public int batchRemoveCategory(Long[] ids) {
		
		return categoryDao.batchRemoveCategory(ids);
	}

	@Override
	public CategoryDO queryCategoryByPid(Long id) {
		
		return categoryDao.queryCategoryByPid(id);
	}


}
