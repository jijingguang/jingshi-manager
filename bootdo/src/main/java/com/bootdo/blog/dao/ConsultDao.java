package com.bootdo.blog.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.bootdo.blog.domain.ConsultDO;
import com.bootdo.blog.domain.ConsultModel;

@Mapper
public interface ConsultDao {
	
	List<ConsultModel> queryConsultList(Map<String, Object> params);
	
	int queryCountConsultList(Map<String, Object> params);
	
	List<ConsultDO> list(Map<String,Object> map);
	
	int count(Map<String,Object> map);

}
