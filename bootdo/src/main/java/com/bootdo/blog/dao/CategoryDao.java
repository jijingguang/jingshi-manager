package com.bootdo.blog.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.bootdo.blog.domain.CategoryDO;

@Mapper
public interface CategoryDao {
	
	List<CategoryDO> queryCategoryList(Map<String, Object> map);
	
	int queryCountArticleList(Map<String, Object> map);
	
	int addCategory(CategoryDO category);
	
	CategoryDO getCategory(Long id);
	
	int updateCategory(CategoryDO category);
	
	int removeCategory(Long id);
	
	int batchRemoveCategory(Long[] ids);
	
	CategoryDO queryCategoryByPid(Long id);
	
}
