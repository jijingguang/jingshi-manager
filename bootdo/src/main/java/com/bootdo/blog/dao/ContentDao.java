package com.bootdo.blog.dao;

import com.bootdo.blog.domain.ArticleDO;
import com.bootdo.blog.domain.ContentDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 文章内容
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 16:17:48
 */
@Mapper
public interface ContentDao {

	ContentDO get(Long cid);
	
	ArticleDO getArticle(Long id);
	
	List<ContentDO> list(Map<String,Object> map);
	
	int count(Map<String,Object> map);
	
	int save(ContentDO content);
	
	int update(ContentDO content);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
	
	List<ArticleDO> queryArticleList(Map<String, Object> map);
	
	int queryCountArticleList(Map<String, Object> map);
	
	int removeArticle(Long id);
	
	int saveArticle(ArticleDO article);
	
	int updateArticle(ArticleDO article);
	
	int batchRemoveArticle(Long[] ids);
}
