package com.bootdo.blog.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 拆迁咨询表
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2019-07-01 16:55:41
 */
public class ConsultDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//姓名
	private String name;
	//手机号
	private String mobile;
	//动迁范围 0 其他 1 棚户区改造 2 宅基地 3 楼房 4 环保拆迁 5养殖场 6商铺 7违建拆除 8厂房 9企业
	private Integer removeScope;
	//动迁进度 0 未拆 1 已拆
	private Integer removeSchedule;
	//房产手续 0 有 1 无
	private Integer propertyFormalities;
	//创建时间
	private Date createTime;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：姓名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：手机号
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：动迁范围 0 其他 1 棚户区改造 2 宅基地 3 楼房 4 环保拆迁 5养殖场 6商铺 7违建拆除 8厂房 9企业
	 */
	public void setRemoveScope(Integer removeScope) {
		this.removeScope = removeScope;
	}
	/**
	 * 获取：动迁范围 0 其他 1 棚户区改造 2 宅基地 3 楼房 4 环保拆迁 5养殖场 6商铺 7违建拆除 8厂房 9企业
	 */
	public Integer getRemoveScope() {
		return removeScope;
	}
	/**
	 * 设置：动迁进度 0 未拆 1 已拆
	 */
	public void setRemoveSchedule(Integer removeSchedule) {
		this.removeSchedule = removeSchedule;
	}
	/**
	 * 获取：动迁进度 0 未拆 1 已拆
	 */
	public Integer getRemoveSchedule() {
		return removeSchedule;
	}
	/**
	 * 设置：房产手续 0 有 1 无
	 */
	public void setPropertyFormalities(Integer propertyFormalities) {
		this.propertyFormalities = propertyFormalities;
	}
	/**
	 * 获取：房产手续 0 有 1 无
	 */
	public Integer getPropertyFormalities() {
		return propertyFormalities;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
