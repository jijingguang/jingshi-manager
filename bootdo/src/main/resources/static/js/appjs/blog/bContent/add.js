$().ready(function() {
	

	$('.summernote').summernote({
		height : '220px',
		lang : 'zh-CN',
		callbacks: {
            onImageUpload: function(files, editor, $editable) {
                sendFile(files);
            }
        }
	});
	selectLoad();
	validateRule();
});


$.validator.setDefaults({
	submitHandler : function() {
		save(1);
	}
});

function selectLoad() {
	var html = "";
	$.ajax({
		url : '/blog/bContent/category',
		success : function(data) {
			//加载数据
			for (var i = 0; i < data.length; i++) {
				html += '<option value="' + data[i].id + '">' + data[i].categoryName + '</option>'
			}
			$(".chosen-select").append(html);
			$(".chosen-select").chosen({
				maxHeight : 200
			});
			//点击事件
			$('.chosen-select').on('change', function(e, params) {
				console.log(params.selected);
				var opt = {
					query : {
						categoryId : params.selected,
					}
				}
				$('#exampleTable').bootstrapTable('refresh', opt);
			});
		}
	});
}

//function save() {
//    var formData = new FormData($("#signupForm")[0]);
//    $.ajax({
//		type : "POST",
//		url : "/activiti/process/save",
//        data: formData,
//        async: false,
//        cache: false,
//        contentType: false,
//        processData: false,
//		error : function(request) {
//			parent.layer.alert("Connection error");
//		},
//		success : function(data) {
//			if (data.code == 0) {
//				parent.layer.msg("操作成功");
//				parent.reLoad();
//				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
//				parent.layer.close(index);
//
//			} else {
//				parent.layer.alert("操作失败")
//			}
//
//		}
//	});
function save(status) {
//	$("#status").val(status);
	var content_sn = $("#content_sn").summernote('code');
	$("#content").val(content_sn);
	var formData = new FormData($("#signupForm")[0]);
	$.ajax({
		cache : true,
		type : "POST",
		url : "/blog/bContent/save",
		data: formData,
		async : false,
		cache: false,
	    contentType: false,
	    processData: false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(r) {
			if (r.code == 0) {
				parent.layer.msg(r.msg);
				parent.reLoad();
				$("#id").val(r.id);
				parent.layer.closeAll();
			} else {
				parent.layer.alert(r.msg)
			}
		}
	});
}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			articleTitle : "required",
			labTitle : "required"
		},
		messages : {
			articleTitle : "请填写文章标题",
			labTitle : "请填写标签页标题"
		}
	});
}

function returnList() {
	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
	parent.layer.close(index);
}